import createDataContext from './createDataContext'
import {AsyncStorage} from 'react-native'
import {navigate} from '../navigationRef'

const authReducer = (state,action) => {
    switch(action.type) {
        case 'add_error':
            return {...state,errorMessage:action.payload}
        case 'signup':
            return {errorMessage:'',token: action.payload}
        case 'signin':
            return {errorMessage:'',token: action.payload}
        case 'clear_error_message':
            return {...state,errorMessage:''}
        default:
            return state;
    }
}

const signup = (dispatch) => {
    return async ({email, password})=>{
        //make api request
        try{
            await AsyncStorage.setItem('token','42425treggdfgd')
            dispatch({type:'signup',payload:'42425treggdfgd'})
            navigate('TrackList')
            
        }catch(err){
            alert(err)
            //dispatch({type:'add_error',payload:'ERORR'})
        }
        //if we sign up,modify our state

        //if sign up fails, we probably need to reflect an error
    }
}

const tryLocalSignin = dispatch => async() => {
    const token = await AsyncStorage.getItem('token');
    if(token){
        dispatch({type:'signin',payload:token});
        navigate('TrackList');
    }else{
        navigate('Signup')
    }
}

const clearErrorMessage = dispatch => () =>{
    dispatch({type:'clear_error_message'})
}

const signin = (dispatch) => {
    return async ({email,password}) => {
        //try to sign in
        //handle success update state

        //handle failure showing errormessage
        try{
            await AsyncStorage.setItem('token','42425treggdfgd')
            dispatch({type:'signin',payload:'42425treggdfgd'})
            navigate('TrackList')
            
        }catch(err){
            alert(err)
            //dispatch({type:'add_error',payload:'ERORR'})
        }
    }
}

const signout = (dispatch) => {
    return ()=>{
        //sign out!
    }
}

export const { Provider,Context} = createDataContext(
    authReducer,
    {signin,signout,signup, clearErrorMessage,tryLocalSignin},
    {token:null,errorMessage:''}
) 