import React,{useContext} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import {NavigationEvents} from 'react-navigation'
import {Context as AuthContext} from '../context/AuthContext'
import AuthForm from '../components/AuthForm'
import NavLink from '../components/NavLink'


const SigninScreen = () => {

  const {state,signin, clearErrorMessage} = useContext(AuthContext);
//onwillfocus transition on screen is called
//ondidfocus landing on screen is called
//onwillblur and ondidblur when away from screen
    return (
      <View style={styles.container}>
        <NavigationEvents
          onWillBlur={clearErrorMessage}
        />
        <AuthForm
          headerText="Sign in to your Account"
          errorMessage={state.errorMessage}
          onSubmit={signin}
          submitButtonText="Sign In"
        />
        <NavLink 
          text="Dont have an account? sign up instead"
          routeName="Signup"
        />
      </View>
    )
};

SigninScreen.navigationOptions = () =>{
  return {
    header:null
  }
}

const styles = StyleSheet.create({
  container : {
    flex:1,
    justifyContent:'center',
    marginBottom:150
  }
})

export default SigninScreen;